import React from "react"
import {
  FaLinkedin,
  FaTwitterSquare,
  FaGitlab
} from "react-icons/fa"

const data = [
  {
    id: 1,
    icon: <FaLinkedin className="social-icon"></FaLinkedin>,
    url: "https://www.linkedin.com/in/marius-%C5%A1%C4%8Derbickas-99359b10a/",
  },
  {
    id: 2,
    icon: <FaTwitterSquare className="social-icon"></FaTwitterSquare>,
    url: "https://www.twitter.com/KotLabs",
  },
  {
    id: 3,
    icon: <FaGitlab className="social-icon"></FaGitlab>,
    url: "https://gitlab.com/kotLabs"
  }
]
const links = data.map(link => {
  return (
    <li key={link.id}>
      <a href={link.url} className="social-link">
        {link.icon}
      </a>
    </li>
  )
})

export default ({ styleClass }) => {
  return (
    <ul className={`social-links ${styleClass ? styleClass : ""}`}>{links}</ul>
  )
}
