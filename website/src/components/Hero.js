import React from "react"
import Image from "gatsby-image"
import {Link, useStaticQuery} from "gatsby"
import { graphql } from "gatsby"
import SocialLinks from "../constants/socialLinks"

const query = graphql`
  {
    hero: allStrapiHero {
      nodes {
        title
        subtitle
        image {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`

const Hero = () => {
  const data = useStaticQuery(query)
  const {
    hero: { nodes }
  } = data
  const { title, subtitle, image } = nodes[0]

  return (
    <header className="hero">
      <div className="section-center hero-center">
        <article className="hero-info">
          <div>
            <div className="underline"/>
            <h1>{title}</h1>
            <h4>{subtitle}</h4>
            <Link to="/contact" className="btn">
              contact me
            </Link>
            <SocialLinks/>
          </div>
        </article>
        <Image fluid={image.childImageSharp.fluid} className="hero-img"/>
      </div>
    </header>
  )
}


export default Hero
