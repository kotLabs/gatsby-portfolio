## Setup Notes

1. All components ready to go (including imports)
2. Use main.css - less imports
3. Limit amount of components - better overview
4. React Icons

[react icons] :https://react-icons.github.io/react-icons/

```javascript
import { FaHome } from "react-icons/fa"
const Component = () => {
  return <FaHome className="icon"></FaHome>
}
```

5. Use constants to avoid repetition.

6. Docker image build. 
Backend should be running on hosts localhost:1337, so Gatsby could query data.
Will work only on Linux as Mac and Windows don't support. ```--network host```

```shell script
docker build --network host -t registry.gitlab.com/kotlabs/gatsby-portfolio:0.1 .

```